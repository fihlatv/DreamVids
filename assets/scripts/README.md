# DreamVids - Scripts

> Le développement JavaScript de la v2 nécessite [quelques dépendances à installer](https://github.com/DreamVids/DreamVids/tree/dreamvids-2.0-dev/assets)

Chaque fichier JavaScript devra contenir un header la forme ci-dessous.

```js
/**
 * path/to/file.js
 * 
 * Script description
 */
```