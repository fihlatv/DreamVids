function unFlagVideo(vidId) {
	if(confirm("Voulez-vous annuler le report de cette video ?")) {
		$.ajax({
			type: "PUT",
			url: _webroot_ + 'videos/' + vidId,
			data: { flag: false },
			success: function(result) {
				window.location.reload();
			}
		});
	}
}

function flagVideo(vidId) {
	if(confirm("Voulez-vous report cette video ?")) {
		$.ajax({
			type: "PUT",
			url: _webroot_ + 'videos/' + vidId,
			data: { flag: true },
			success: function(result) {
				window.location.reload();
			}
		});
	}
}

function suspendVideo(vidId) {
	if(confirm("Voulez-vous vraiment suspendre cette video ?")) {
		$.ajax({
			type: "PUT",
			url: _webroot_ + 'videos/' + vidId,
			data: { suspend: true },
			success: function(result) {
				window.location.reload();
			}
		});
	}
}

function unSuspendVideo(vidId) {
	if(confirm("Voulez-vous vraiment annuler la suspension de cette video ?")) {
		$.ajax({
			type: "PUT",
			url: _webroot_ + 'videos/' + vidId,
			data: { suspend: false },
			success: function(result) {
				window.location.reload();
			}
		});
	}
}

function eraseVideo(vidId) {
	if(confirm("Voulez-vous vraiment effacer cette video DEFINITIVEMENT ?")) {
		$.ajax({
			type: "DELETE",
			url: _webroot_ + 'videos/' + vidId,
			data: {},
			success: function(result) {
				window.location.reload();
			}
		});
	}
}



function unflagComment(commentId) {
	if(confirm("Voulez-vous vraiment annuler le report de ce commentaire ?")) {
		$.ajax({
			type: "PUT",
			url: _webroot_ + 'comments/' + commentId,
			data: { flag: false },
			success: function(result) {
				window.location.reload();
			}
		});
	}
}

function eraseComment(commentId) {
	if(confirm("Voulez-vous vraiment effacer ce commentaire DEFINITIVEMENT ?")) {
		$.ajax({
			type: "DELETE",
			url: _webroot_ + 'comments/' + commentId,
			data: {},
			success: function(result) {
				window.location.reload();
			}
		});
	}
}

function deleteEgg(egg_id){
		if(confirm("ATTENTION ! Supprimer un oeuf déjà trouvé entraine la supression des points de la personne qu'il l'a trouvé.")){
			$.ajax({
				type: "DELETE",
				url: _webroot_ + 'admin/egg/' + egg_id,
				data: {},
				success: function(result) {
					window.location.reload();
				}
			});
		}			
}

function checkSpace(id, srv){
	var element = document.getElementById(id);
	if(element){
			$.ajax({
				url: _webroot_ + 'admin/statistic/space/' + srv,
				success: function(result){
					if(result.error){
						element.innerHTML = result.error;
					}else{
						var sizes = {
							1: '',
							1000: 'K',
							1000000: 'M',
							1000000000: 'G',
							1000000000000: 'T',
							1000000000000000: 'P',
							1000000000000000000: 'E'
						}
						var unity = 1;
						for(var size in sizes){
							if(result.empty_space/(size) > 1){
								unity = size;
							}
						}
						element.innerHTML = (result.empty_space/unity).toFixed(0) + sizes[unity] + "o";
					}
				}
			});
	}
}
if(typeof servers !== 'undefined'){
	(function(servers){
		servers.forEach(function(v,i,ar){
			checkSpace(v+'_space', v);
		})
	})(servers);
}

$('#news_modal').on('show.bs.modal', function (event) {
	
	var button = $(event.relatedTarget)
	
	var title = button.data('title');
	var content = button.data('content');
	var level = button.data('level');
	var icon = button.data('icon');
	
	var modal = $(this);
	var form = $("#news_form");
	
	form.data('method', button.data('method'));
	switch(button.data('method')){
		case 'PUT' :
						form.data('id', button.data('id'));
						modal.find('#input-level option[value="'+level+'"]').prop('selected', true);
						modal.find('.modal-title').text('Modifier la news "' + title + '"');
						modal.find('.modal-body #input-title').val(title);
						modal.find('.modal-body #input-content').val(content);
						modal.find('.modal-body #input-icon').val(icon);
			break;
		case 'POST' : 
						modal.find('.modal-title').text('Ajouter une news');
						modal.find('#input-level option[value=""]').prop('selected', true);
						modal.find('.modal-body #input-title').val('');
						modal.find('.modal-body #input-content').val('');
						modal.find('.modal-body #input-icon').val('');
						form.data('id', '');
			break;
	}
});

$('#news_form').submit(function(event) {
	
	var form_data = {};
	$(this).serializeArray().forEach(function(v){
		form_data[v.name] = v.value;
	});
	$.ajax({
		method: $(this).data('method'),
		data:form_data,
		url: _webroot_ + 'admin/news/' + $(this).data('id'),
		success: function(result) {
				if(result.success == true){
					window.location.reload();
				}else{
					var alert_el = document.createElement('div');
					alert_el.innerHTML = '<div class="alert alert-danger fade in" role="alert"><strong>Oups !</strong> Une erreur s\'est produite lors de l\'envoi</div>';
					$('#news_modal').find(".modal-header").append(alert_el);
					
					setTimeout(function(){
						$(alert_el).fadeOut();
					}, 2000);
				}
		}
	});
	
	
	event.preventDefault();
});

function deleteNew(el){
	if(confirm('Supprimer cette news ?')){
		$.ajax({
			method: 'delete',
			url: _webroot_ + 'admin/news/' + el.dataset.id,
			success: function(result) {
					window.location.reload();
			}
		});
		
	}
}

$('.push-bullet-btn').click(function(e){
	$.ajax({
			method: 'put',
			data:{enable:$(this).data('value')},
			url: _webroot_ + 'admin/notifications/enable',
			success: function(result) {
					window.location.reload();
			}
		});
});

$('.push-bullet-btn-send').click(function(e){
	$.ajax({
			method: 'post',
			url: _webroot_ + 'admin/notifications/',
			success: function(result) {
					alert('Fait !');
			}
		});
});

$('#send-private-notif-btn').click(function(e){
	e.preventDefault();
	$(this).attr('disabled', 'disabled');
	$(this).innerHTML = 'Envoi ... ';
	var send_to = $('#send-private-notif-to');
	var send_to_name = send_to.find('option[value="'+send_to.val()+'"]');
	var send_content = $('#send-private-notif-content');
	var send_level = $('#send-private-notif-level');
	var send_force = $('#send-private-notif-force-push');
	
	var btn = $(this);
	var post_data = {
		to:send_to.val(),
		content: send_content.val(),
		level: send_level.val(),
		force_push: send_force.prop('checked') ? 1 : 0,
		type: 'private'
	};
	
	if(post_data.to == 'send_to_all'){
		post_data.type = 'broadcast';
	}
	
	var alert_el = $('.msg-container');
	$.ajax({
			method: 'post',
			data: post_data,
			url: _webroot_ + 'admin/notifications/',
			success: function(result) {
					btn.removeAttr('disabled');
					if(result.success){
						alert_el.html('<div class="alert alert-success fade in" role="alert"><strong>Fait !</strong> message envoyé à '+send_to_name.text()+'</div>');
						send_to.val('');
						send_level.val('');
						send_content.val('');
						send_force.prop('checked', false)
					}else{
						btn.innerHTML = 'ERREUR ... Cliquez pour réessayer';
						
					}
			}
		});
});