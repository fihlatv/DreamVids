# DreamVids - Style

> Le développement Sass de la v2 nécessite [quelques dépendances à installer](https://github.com/DreamVids/DreamVids/tree/dreamvids-2.0-dev/assets)

Chaque fichier Sass devra contenir un header la forme ci-dessous.

```css
/**
 * path/to/file.js
 *
 * Stylesheet description
 */
```