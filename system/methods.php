<?php

abstract class Method {

	const INVALID = "INVALID";
	const GET = "GET";
	const POST = "POST";
	const PUT = "PUT";
	const DELETE = "DELETE";

}