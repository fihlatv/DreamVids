<?php

abstract class Action {

	const INDEX = 'INDEX';
	const GET = 'GET';
	const CREATE = 'CREATE';
	const UPDATE = 'UPDATE';
	const DESTROY = 'DESTROY';

}