<div class="middle">
	<h1 class="title">Chasse aux oeufs<a href="<?php echo WEBROOT . 'egg/rank';?>">Voir le classement</a></h1>
	
		<p>
			Le but de la chasse aux oeufs est de trouver un maximum d'oeuf.
			A chaque oeuf, vous gagnez 1 point ou 3 point si c'est un oeuf en or. 
			Ce concours est organisé en partenariat avec la <a href="http://cavicon.fr/" target="_blank">CAVIcon</a>, ce qui signifie que vous pouvez aussi chercher les oeufs sur <a href="http://cavicon.fr/" target="_blank">leur site</a> !
			Les cinq personnes qui gagnent le plus d'oeufs gagneront : 
		</p>
		<p class="center">
			<ol>
				<li>Le 1<sup>er</sup> : un <b>billet 1 jour</b> pour la <a href="http://cavicon.fr/" target="_blank">CAVIcon</a></li>
				<li>Les 2<sup>e</sup> et 3<sup>e</sup> : un <b>pass bonbon</b> ET un <b>pass boisson</b> pour la <a href="http://cavicon.fr/" target="_blank">CAVIcon</a>
					(<a href="http://cavicon.fr/infos#pass" target="_blank">plus d'infos : ici</a>)
				</li>

				<li>Les 4<sup>e</sup> et 5<sup>e</sup> : un <b>pass bonbon</b> OU un <b>pass boisson</b> au choix pour la <a href="http://cavicon.fr/" target="_blank">CAVIcon</a>
					(<a href="http://cavicon.fr/infos#pass" target="_blank">plus d'infos : ici</a>)
				</li>

			</ol>
			
		</p>	
		<p class="center">
			<img src="<?php echo IMG . 'eggs/egg_normal.png'; ?>">		
		</p>	
	
</div>
