<section class="error-404">
	
	<div class="illustration">
		<div class="cloud">403</div>

		<ul class="rain">
			<li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
		</ul>
	</div>

	<div class="text">
		<div class="inner-text">
			<h1>Accès interdit</h1>
			L'accès à cette page a été interdit par un administrateur. Vous pouvez faire une recherche ou retourner à l'<a href="<?php echo WEBROOT; ?>">accueil</a>.<br /><br />
		</div>
	</div>

</section>