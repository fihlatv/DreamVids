<section class="error-404">
	
	<div class="illustration">
		<div class="cloud">500</div>

		<ul class="rain">
			<li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
		</ul>
	</div>

	<div class="text">
		<div class="inner-text">
			<h1>Erreur interne</h1>
			Un soucis semble être présent de notre côté. Il y a de grande chances qu'un développeur se soit senti pousser des ailes et ait fait n'importe quoi. Il est actuellement en train de se faire torturer dans une succursalle de DreamVids, à Guantánamo. Vous pouvez envoyer vos dons pour que son calvaire cesse<?php if($can_go_to_home){ ?>, faire une recherche ou retourner à l'<a href="<?php echo WEBROOT; ?>">accueil</a><?php } ?>.<br /><br />
		</div>
	</div>

</section>