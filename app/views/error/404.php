<section class="error-404">
	
	<div class="illustration">
		<div class="cloud">404</div>

		<ul class="rain">
			<li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
		</ul>
	</div>

	<div class="text">
		<div class="inner-text">
			<h1>Perdu sur DreamVids ?</h1>
			Il semblerait que la page que vous cherchez n'existe pas ou plus :/ Vous pouvez faire une recherche ou retourner à l'<a href="<?php echo WEBROOT; ?>">accueil</a>.<br /><br />
		</div>
	</div>

</section>