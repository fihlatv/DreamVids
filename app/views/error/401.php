<section class="error-404">
	
	<div class="illustration">
		<div class="cloud">401</div>

		<ul class="rain">
			<li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li>
		</ul>
	</div>

	<div class="text">
		<div class="inner-text">
			<h1>Accès non autorisé</h1>
			Vous devez disposer d'un accès spécial pour accéder à cette page. Vous pouvez faire une recherche ou retourner à l'<a href="<?php echo WEBROOT; ?>">accueil</a>.<br /><br />
		</div>
	</div>

</section>