<section class="middle">
	<h3 class="title">Rechercher - "<?php echo $search; ?>"</h3>
	<div class="message error">
			<div class="message-icn"><img src="<?php echo IMG . 'message_'.$error['level'].'_icon.png'; ?>" alt="Message d'erreur"></div>
			<p><?php echo $error['message']; ?></p>
		</div>
	
</section>
