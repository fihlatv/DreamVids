<div class="content">
    <h1 class="title">Conditions Générales d’Utilisation du service « DreamVids »</h1>

    <p style="text-align:justify;">
        <h3 class="title">1. MENTIONS LEGALES</h3>
        <p>
            Le site web dreamvids.fr est édité par l'Association loi 1901 "DREAMVIDS" (Identification NRA:
            W832012749), déclarée le 12/09/2014 et parue au journal officiel le 20/09/2014.<br />
            <br />
            SIEGE SOCIAL:<br />
            Chemin du plan,<br />
            Quartier le Pavillon<br />
            83660 Carnoules.
        </p>

        <h3 class="title">2. PRÉSENTATION DU SITE</h3>
        <p>
            DreamVids est un site de partage de vidéos associatif permettant de visionner, accéder, mettre en
            ligne, stocker et partager des vidéos (le "Site"). L'espace de stockage alloué par l'utilisateur est
            illimité (sauf notification contraire par courrier électronique), la taille de chaque vidéo étant
            restreinte à 2Go. Conformément aux pratiques et usages de l'internet, de la publicité peut être
            insérée au sein du Site.
        </p>

        <h3 class="title">3. VOTRE PROPRIÉTÉ INTELLECTUELLE</h3>
        <p>
            Si vous êtes inscrit et que vous détenez un compte par le biais duquel vous mettez en ligne vos
            vidéos, ainsi que vos commentaires, votre pseudonyme et votre avatar ("Votre Contenu"),
            DreamVids n'acquiert aucun droit de propriété sur Votre Contenu.
            Dès lors que vous rendez accessible Votre Contenu à d'autres utilisateurs (individuellement ou par
            groupe), ceux-ci disposent, à titre gratuit et à des fins exclusivement personnelles, de la faculté de
            visualiser et partager votre contenu sur le Site ou à partir du Site, sur d'autres supports de
            communications électroniques (notamment, les smartphones, tablettes, télévisions connectées et
            consoles de jeu) et ce, pendant toute la durée de l'hébergement de Votre Contenu sur le Site.
            Vous êtes par ailleurs informé que, compte tenu des caractéristiques intrinsèques de l'internet, les
            données transmises, notamment Votre Contenu, ne sont pas protégées contre les risques de
            détournement et/ou de piratage, ce dont nous ne saurions être tenus responsables. Il vous
            appartient, le cas échéant, de prendre toutes les mesures appropriées de façon à protéger ces
            données.
        </p>

        <h3 class="title">4. NOTRE PROPRIÉTÉ INTELLECTUELLE</h3>
        <p>
            Le site est notre propriété exclusive. D'une manière générale, nous vous accordons un droit
            gratuit, personnel, non-exclusif et non-transférable d'accès et d'utilisation du Site sous réserve de
            votre acceptation et respect des CGU.
        </p>

        <h3 class="title">5. CARACTERE "LIBRE ET OPEN-SOURCE" DU SITE</h3>
        <p>
            Tous les composants du Site (exceptés Votre Contenu): codes,
            images et textes sont gratuitement mis à disposition de tout-un-chacun selon les termes de la
            <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas
            d’Utilisation Commerciale - Partage dans les Mêmes Conditions (CC BY-NC-SA)</a> sur le site web www.github.com.
        </p>

        <h3 class="title">6. NOTRE RESPONSABILITÉ D'HÉBERGEUR</h3>
        <p>
            Nous ne sommes légalement tenus à aucune obligation générale de surveillance du contenu
            transmis ou stocké via le Site. Les seules obligations inhérentes à notre qualité d'hébergeur
            concernent :
            <ul>
                <li>- La lutte contre certains contenus selon la procédure décrite à la rubrique Signaler un  contenu</li>
                <li>- La conservation de vos données de connexion, par ailleurs couvertes par le secret professionnel et
                    traitées dans le respect des dispositions légales en matière de Données Personnelles</li>
                <li>- Le retrait de tout contenu manifestement illicite, dès lors que nous en aurons eu effectivement
                    connaissance.</li>
            </ul>
        </p>

        <h3 class="title">7. VOTRE RESPONSABILITÉ D'UTILISATEUR</h3>
        <p>
            En fournissant Votre Contenu sur le Site (qu'il s'agisse de vidéos, de commentaires que vous y
            apportez, de votre pseudo ou de votre avatar), vous êtes tenus au respect des dispositions légales
            et réglementaires en vigueur. Il vous appartient en conséquence de vous assurer que le stockage
            et la diffusion de ce contenu via le Site ne constitue pas:
            <ul>
                <li>- Une violation des droits de propriété intellectuelle de tiers (notamment, clips, émissions de télévision,
                    courts, moyens et /ou longs métrages, animés ou non, publicités, que vous n'avez pas réalisés personnellement
                    ou pour lesquels vous ne disposez pas des autorisations nécessaires des tiers ou de sociétés de gestion
                    collective, titulaires de droits sur ceux-ci)</li>
                <li>- Une atteinte aux personnes (notamment diffamation, insultes, injures, etc.) et au respect de la vie privée</li>
                <li>- Une atteinte à l'ordre public et aux bonnes moeurs (notamment, apologie des crimes contre l'humanité,
                    incitation à la haine raciale, pornographie enfantine, etc.).</li>
            </ul>
            A défaut, Votre Contenu sera retiré dans les conditions visées au paragraphe 5 et/ou votre compte
            désactivé sans formalité préalable. En outre, vous encourrez, à titre personnel, les sanctions
            pénales spécifiques au contenu litigieux, outre la condamnation éventuelle au paiement de
            dommages et intérêts.
            Par ailleurs, en tant qu’utilisateur du Site, vous vous engagez à:
            <ul>
                <li>- Ne pas accroître artificiellement (vous-mêmes ou par l’intermédiaire d’un tiers ainsi qu’automatiquement
                    ou manuellement) le nombre de vues, d'impressions et/ou de clics associés tant aux vidéos des Autres
                    Utilisateurs qu’à votre Contenu</li>
                <li>- Ne pas inciter d'autres personnes ni leur proposer de récompense financière en vue d’accroître
                    artificiellement le nombre de vues, d'impressions et/ou de clics associés aux vidéos des Autres Utilisateurs
                    ou à votre Contenu.</li>
            </ul>
        </p>

        <h3 class="title">8. INSCRIPTION ET ACCÈS</h3>
        <p>
            Aux fins de bénéficier des fonctionnalités du Site, vous devez créer un compte au moyen du
            formulaire en ligne prévu à cet effet. Conformément à la loi applicable n° 78-17 du 6 janvier 1978 relative
            à l'informatique, aux fichiers et aux libertés, telle qu’amendée le 6 août 2004, vous disposez d'un droit
            d'accès, de modification, de rectification et de suppression des informations vous concernant. Vous
            disposez en outre d’un droit d’opposition, pour motifs légitimes, au traitement de ces données.
            Par ailleurs, en tant que contributeur au Site, il est de votre responsabilité
            de vous assurer que les données personnelles permettant de vous identifier soient exactes et
            complètes. L'accès à votre compte s'effectue par saisie de votre identifiant et mot de passe associé,
            dont vous assurez seul la confidentialité. En cas de non-respect des obligations inhérentes à votre
            responsabilité ci-avant, l'accès à votre espace personnel peut être, immédiatement et sans préavis,
            temporairement ou définitivement suspendu au moyen de la désactivation de votre compte et ce, sans
            préjudice de nos autres droits.
        </p>

        <h3 class="title">9. DISPONIBILITÉ DU SITE</h3>
        <p>
            Le Site est par principe accessible 24/24h, 7/7j, sauf interruption, programmée ou non, pour les
            besoins de sa maintenance ou cas de force majeure. Etant de fait soumis à une obligation de
            moyens, nous ne saurions être tenus responsable de tout dommage, quelle qu'en soit la nature,
            résultant d'une indisponibilité du Site.
        </p>

        <h3 class="title">10. PREUVE, CONSERVATION ET ARCHIVAGE</h3>
        <p>
            Les registres informatisés conservés dans nos systèmes dans le respect des règles de l'art en
            matière de sécurité, seront considérés comme preuves des communications de courriers
            électroniques, envois de formulaire d'inscription, téléchargements de vidéos et postages de
            commentaires. L'archivage des formulaires d'inscription est effectué sur un support de nature à
            assurer le caractère fidèle et durable requis par les dispositions légales en vigueur. Il est convenu
            qu'en cas de divergence entre nos registres informatisés et les documents au format papier ou
            électronique dont vous disposez, nos registres informatisés feront foi.
        </p>
    </p>
</div>