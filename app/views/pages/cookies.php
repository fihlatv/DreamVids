<div class="content">
    <h1 class="title">Politique d'utilisation des cookies</h1>

    <p style="text-align: justify;">
        DreamVids utilise les cookies à des fins de sauvegarde de vos sessions de connexion et d'analyse d'audience.
    </p>

    <h3 class="title">Un cookie, qu'est-ce que c'est ?</h3>
    <p>
        Les cookies sont des fichiers texte d'informations téléchargés sur votre ordinateur de bureau,
        ordinateur portable ou appareil mobile personnel (collectivement un « Appareil ») quand vous visitez un site web.
        Votre navigateur web renvoie ensuite ces cookies vers le site web qui les a générés lors de chaque visite. Il peut également
        les envoyer vers un autre site web qui reconnaît ces cookies. Les cookies apportent une aide précieuse et leur
        utilisation peut avoir plusieurs objectifs. Ils vous permettent notamment de naviguer plus efficacement d'une page à
		l'autre en mémorisant vos préférences et en en accélérant votre interaction avec le site web.
		<br />
		Vous pouvez en apprendre davantage sur les cookies sur <a href="http://www.allaboutcookies.org">www.allaboutcookies.org</a> et
        <a href="http://www.youronlinechoices.eu">www.youronlinechoices.eu</a>. 
    </p>

    <h3 class="title">Sauvegarde de vos sessions de connexion</h3>
    <p style="text-align: justify;">
		Lorsque vous vous connectez sur DreamVids, nous avons besoin de stocker un cookie sur votre ordinateur afin que le site se souvienne de vous lorsque vous changerez de page.
		Ce cookie a une durée de vie d'un an (afin que vous n'ayez pas à vous reconnecter lors de chacune de vos visites) et ne sert en aucun cas à
        récolter une quelconque information personnelle vous concernant. Le contenu de ce cookie est un identifiant (un nombre
        très grand) permettant de retrouver votre session et d'y associer toutes les informations de votre compte.
    </p>

    <h3 class="title">Analyse d'audience</h3>
    <p style="text-align: justify;">
        Afin d'analyser le traffic du site, nous utilisons un prestataire de service (Google Analytics) qui stocke plusieurs
        cookies sur votre ordinateur afin de pouvoir suivre votre activité sur le site et nous permettre d'optimiser l'expérience
        de nos utilisateurs: En effet, cet outil nous génère des rapports d'analyse complets sur le comportement de nos
        utilisateurs (pages visitées, durée de la visite, recurrence, navigateur, système d'exploitation, site référent, etc).
        Néanmoins, ces informations sont récoltées de manière totalement anonyme: nous ne sommes pas en mesure de savoir
        que vous avez visité le site tel jour à telle heure, seulement qu'un visiteur (dont nous ne savons rien) a visité le site
        tel jour à telle heure.
    </p>

    <h3 class="title">Le cookie d'acceptation des cookies</h3>
    <p style="text-align: justify;">
        La loi nous oblige à avertir les internautes lorsque nous utilisons les cookies. Le fait est que pour
        éviter de vous redemander d'accepter l'utilisation des cookies à chaque fois que vous visitez une page, nous avons
        -justement- besoin d'un cookie pour nous rapeller que vous avez déjà accepté l'utilisation des cookies.
    </p>
</div>
