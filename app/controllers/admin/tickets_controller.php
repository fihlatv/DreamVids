<?php
require_once SYSTEM.'controller.php';
require_once SYSTEM.'actions.php';
require_once SYSTEM.'view_response.php';
require_once SYSTEM.'redirect_response.php';
require_once SYSTEM.'view_message.php';

require_once MODEL.'ticket.php';
require_once MODEL.'conversation.php';
require_once MODEL.'message.php';

class AdminTicketsController extends AdminSubController {
	public function __construct() {
		$this->denyAction(Action::GET);
		$this->denyAction(Action::CREATE);
		$this->denyAction(Action::DESTROY);
	}
	
	public function index($request) {
		$data = [];
		$data['tickets'] = Session::get()->getAssignedTickets();
		$data['all'] = false;
		return new ViewResponse('admin/tickets/index', $data);
	}
	
	public function all($request){
		$data = [];
		$data['tickets'] = Ticket::find('all');
		$data['all'] = true;
		return new ViewResponse('admin/tickets/index', $data);
	}
	
	public function solved($id, $request) {
		$ticket = Ticket::find($id);
		$message = "Après prise en charge de votre problème, celui-ci a été signalé comme résolu. Si le problème persiste ou qu'un autre survient, merci de nous le faire savoir via l'assistance.";
		$this->mail($ticket, $message);
		if ($ticket->conv_id != '' && Conversation::exists($ticket->conv_id)) {
			Conversation::find($ticket->conv_id)->removeChannel(User::find(User::getIdByName($ticket->tech))->getMainChannel());
		}
		$ticket->delete();
		return new RedirectResponse(WEBROOT.'admin/tickets');
	}
	
	public function inprogress($id, $request) {
		$ticket = Ticket::find($id);
		if ($ticket->tech == '') {
			$ticket->tech = Session::get()->username;
			if (User::exists(array('id' => $ticket->user_id))) {
				$conv = Conversation::createNew('[Assistance] Demande #'.$ticket->id, Session::get()->getMainChannel(), ';'.User::find($ticket->user_id)->getMainChannel()->id.';'.Session::get()->getMainChannel()->id.';', 1, Session::get()->id);
				Message::sendNew(User::find($ticket->user_id)->getMainChannel()->id, $conv, $ticket->description);
				Message::sendNew(Session::get()->getMainChannel()->id, $conv, 'Bonjour, je suis '.StaffContact::getShownName(Session::get()).' et j\'ai pris en charge votre demande d\'assistance. Cette conversation a été créée pour pouvoir discuter avec vous. ATTENTION: Ne communiquez jamais votre mot de passe, même à un technicien ! Un vrai technicien a à sa disposition tous les outils nécessaires à la résolution de votre problème !', 1);
				$ticket->conv_id = $conv;
			}
			$ticket->save();
			$message = "Votre demande d'assistance a été prise en charge par {{tech}}. Vous serez prochainement avertit de l'issue de l'intervention.";
			$this->mail($ticket, $message);
		}
		return new RedirectResponse(WEBROOT.'admin/tickets');
	}
	
	public function bug($id, $request) {
		$ticket = Ticket::find($id);
		$message = "Après étude de votre problème, il en résulte qu'il ne s'agit pas d'un incident isolé mais bien d'un problème technique interne à DreamVids (bug). Nous travaillons actuellement à la détection et à la résolution de ce bug mais nous ne pouvons vous donner de plus amples informations. Nous nous excusons pour la gêne occasionnée et vous remerçions de votre patience.";
		$this->mail($ticket, $message);
		if ($ticket->conv_id != '' && Conversation::exists($ticket->conv_id)) {
			Conversation::find($ticket->conv_id)->removeChannel(User::find(User::getIdByName($ticket->tech))->getMainChannel());
		}
		$ticket->delete();
		return new RedirectResponse(WEBROOT.'admin/tickets');
	}
	
	public function edit_level($id){
		$data['ticket'] = Ticket::find($id);
		$data['levels'] = TicketLevels::find('all');
		$data['levels'] = is_null($data['levels']) ? [] : $data['levels'];
		return new ViewResponse('admin/tickets/edit_level', $data);
	}
	
	public function update($id, $request){
		$param = $request->getParameters();
		$ticket = Ticket::find($id);
		
		if(isset($param['new']) && $param['new']){
			$level = TicketLevels::create([
				'label' => $param['label']
				]);
			$ticket->ticket_levels_id = $level->id;
		}else{
			$ticket->ticket_levels_id = $param['level_id'];
		}
		
		
		$ticket->save();
		StaffNotification::createNotif('ticket_level_change', Session::get()->id, null, $ticket->id);
		$r = $this->index($request);
		$r->addMessage(ViewMessage::success("Modification effectuée"));
		return $r;
	}
	
	private function mail($ticket, $message) {
		if ($ticket->user_id !== '0') {
			$username = (User::exists(array('id' => $ticket->user_id))) ? ' '.User::find($ticket->user_id)->username : '';
			$to = (User::exists(array('id' => $ticket->user_id))) ? User::find($ticket->user_id)->email : $ticket->user_id;
			$subject = '[DreamVids] Avancement de votre demande d\'assistance #'.$ticket->id;
			$message = str_replace('{{tech}}', Utils::secure(StaffContact::getShownName(Session::get())), $message);
			$message = "Bonjour$username,\r\n\r\n$message\r\n\r\nCordialement,\r\nL'équipe DreamVids.";
			$headers = 'From: DreamVids <assistance@dreamvids.fr>';
			mail($to, $subject, utf8_decode($message), $headers);
		}
	}
	
	public function get($id, $request){}
	public function create($request){}
	public function destroy($id, $request){}
}