<?php

require_once SYSTEM.'controller.php';
require_once SYSTEM.'actions.php';
require_once SYSTEM.'view_response.php';
require_once SYSTEM.'view_message.php';
require_once SYSTEM.'redirect_response.php';

class RegisterController extends Controller {

	public function __construct() {
		$this->denyAction(Action::GET);
		$this->denyAction(Action::UPDATE);
		$this->denyAction(Action::DESTROY);
	}

	public function index($request) {
		if(Session::isActive()) {
			return new RedirectResponse(WEBROOT.'login');
		}
		else {
			$data = array();
			$data['currentPageTitle'] = 'Inscription';
			$data["currentPage"] = "register";
			return new ViewResponse('login/register', $data);
		}
	}

	// Called by a POST request
	public function create($request) {
        
		$req = $request->getParameters();
		
		if(isset($req['submitRegister'])) {
			if(isset($req['username'])) {
				if(isset($req['pass'])) {
					if(isset($req['pass-confirm'])) {
						if(isset($req['mail'])) {
						    if(isset($req["g-recaptcha-response"])){
						    	if (isset($req['CGU']) && $req['CGU'] == 'CGU') {
									$data = $_POST;
									$data['currentPageTitle'] = 'Inscription';
									$data["currentPage"] = "register";
							        $url = "https://www.google.com/recaptcha/api/siteverify?secret="; //Adress a get
							        $url .= Config::getValue_("recaptcha_private"); //Cle prive
							        $url .= "&response=" . $req['g-recaptcha-response']; // Resultat de captcha
							        $check_captcha = true;
							        if(isset($req['cavicon'])){
							        	$key = "key";
							        	$check_captcha = $req['cavicon'] != $key;
							        }
									$check_captcha = true; //Override
							        if($check_captcha){
							        	$json_result = json_decode(file_get_contents($url), true); //Parsage de la reponse
							        	if(@$json_result["success"] != true){ 
								            $response = new ViewResponse('login/register', $data);
								            $response->addMessage(ViewMessage::error('Erreur de captcha')); //Affichage de l'erreur
							            
							            	return $response;
							        	}	
							        }
							        else{
			                          //OK
							        }
						    	}
						    	else {
						    		$response = new ViewResponse('login/register', $data);
						    		$response->addMessage(ViewMessage::error('Merci d\'accepter nos conditions avant de vous inscrire'));
						    		
						    		return $response;
						    	}
						    }else{ //Affichage de l'erreur
						        $response = new ViewResponse('login/register', $data);
						        $response->addMessage(ViewMessage::error('Erreur de captcha'));
						        
						        return $response;
						    }
							$username = Utils::secure($req['username']);
							$pass = Utils::secure($req['pass']);
							$pass2 = Utils::secure($req['pass-confirm']);
							$mail = Utils::secure($req['mail']);

							
							if(Utils::validateUsername($username) && Utils::validateMail($mail) && $pass2 != '' && $pass != '') {
								if($pass == $pass2) {
									if(!User::find_by_username($username)) {
										if(!User::isMailRegistered($mail)) {
											User::register($username, $pass, $mail);
											
											$created_user = User::find('first', array('username' => $username));
											$created_user->sendWelcomeNotification();
											
											
											$response = new ViewResponse('login/login');
											$response->addMessage(ViewMessage::success('Inscription validée. Vous pouvez vous connecter !'));
											return $response;
										}
										else {
											$response = new ViewResponse('login/register', $data);
											$response->addMessage(ViewMessage::error('Cette adresse e-mail est déjà enregistrée'));

											return $response;
										}
									}
									else {
										$response = new ViewResponse('login/register', $data);
										$response->addMessage(ViewMessage::error('Ce nom d\'utilisateur est déjà pris'));

										return $response;
									}
								}
								else {
									$response = new ViewResponse('login/register', $data);
									$response->addMessage(ViewMessage::error('Les mots de passe ne correspondent pas'));

									return $response;
								}
							}
							else {
								$response = new ViewResponse('login/register', $data);
								$response->addMessage(ViewMessage::error('Veuillez saisir des informations valides (a-z/A-Z/0-9)'));

								return $response;
							}
						}
						else {
							$response = new ViewResponse('login/register', $data);
							$response->addMessage(ViewMessage::error('L\'adresse e-mail est requise'));

							return $response;
						}
					}
					else {
						$response = new ViewResponse('login/register', $data);
						$response->addMessage(ViewMessage::error('Veuillez confirmer le mot de passe'));

						return $response;
					}
				}
				else {
					$response = new ViewResponse('login/register', $data);
					$response->addMessage(ViewMessage::error('Le mot de passe est requis'));

					return $response;
				}
			}
			else {
				$response = new ViewResponse('login/register', $data);
				$response->addMessage(ViewMessage::error('Le nom d\'utilisateur est requis'));

				return $response;
			}
		}
	}

	public function get($id, $request) {}
	public function update($id, $request) {}
	public function destroy($id, $request) {}

}
