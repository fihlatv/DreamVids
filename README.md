![DreamVids](/img/banniere.png "DreamVids v2")

Core rework & new design for the DreamVids video sharing platform
This project is using [SysDream2](https://github.com/Quadrifoglio/SysDream-2) as a base.

##Dependencies

###Back
This project is using php-activerecord as an ORM library for accessing the database.
To install all required dependencies, just install composer and type "composer install" in a command prompt.

###Front
Le développement front-end necessite quelques [dépendances à installer](https://github.com/dreamvids/DreamVids/blob/dev/assets/README.md).

##Authors

Back-end development: [Peter Cauty](https://github.com/Vetiore), [Quadrifoglio](https://github.com/Quadrifoglio), [Snapcube](https://github.com/Snapcube), [Benjamin Robinet](https://github.com/benjaminrobinet)

Design: [DarkWos](https://twitter.com/darkwos1), [LowiSky](https://github.com/LowiSky)

Front-end development: [Dimou](https://github.com/dimitrinicolas), [LowiSky](https://github.com/LowiSky), [mxcmaxime](https://github.com/mxcmaxime)